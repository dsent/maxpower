//= ../../node_modules/jquery/dist/jquery.js
//= ../../node_modules/bootstrap/dist/js/bootstrap.bundle.js
//= ../../node_modules/slick-carousel/slick/slick.min.js
//= ../../node_modules/magnific-popup/dist/jquery.magnific-popup.min.js

$('.slider-big__video').magnificPopup({
  type: 'iframe'
});

$('.slider-big').on('init', function(event, slick) {
  $('.slider-count__all').text(slick.slideCount);
})

$('.slider-big').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
  $('.slider-count__current').text(nextSlide+1);
})

$('.slider-big').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  asNavFor: '.slider-small',
  adaptiveHeight: true
});

$('.slider-small').slick({
  slidesToShow: 7,
  slidesToScroll: 1,
  asNavFor: '.slider-big',
  dots: false,
  arrows: false,
  centerMode: true,
  focusOnSelect: true
});

$('.review-list').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  adaptiveHeight: true,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 800,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$(document).on('click', 'a', function(event) {
  var href = $(this).attr('href');

  if(href.indexOf('#') === 0) {
    event.preventDefault();

    var offset = $(href).position().top;
    $("html, body").animate({ scrollTop: offset }, "slow");
  }
});

$(".go-bus-1-1").on("click tap", function(){
  var name = $("#bas-1-1 .name").val().trim();
  var email = $("#bas-1-1 .email").val().trim();
  var phone = $("#bas-1-1 .phone").val().trim();
  var type = "one";
  var price = "4 900";
  if (name && email && phone){
    $.ajax({
      url: "handler.php",
      type: "post",
      data: {type: type, name: name, email: email, phone: phone, price: price},
      success: function(){
        document.location = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=staffpay2&InvId=0&Culture=ru&Encoding=utf-8&Description=MaxPowerTest&OutSum=4900&SignatureValue=4ca8d19c4522d9b44f0ff5aaaa667e2b';
      }
    });
  } else {
    alert("Заполните все поля!");
  }
});
$(".go-bus-2-1").on("click tap", function(){
  var name = $("#bas-2-1 .name").val().trim();
  var email = $("#bas-2-1 .email").val().trim();
  var phone = $("#bas-2-1 .phone").val().trim();
  var type = "one";
  var price = "39 900";
  if (name && email && phone){
    $.ajax({
      url: "handler.php",
      type: "post",
      data: {type: type, name: name, email: email, phone: phone, price: price},
      success: function(){
        document.location = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=staffpay2&InvId=0&Culture=ru&Encoding=utf-8&OutSum=39900&SignatureValue=ebca3c93046a59b6d477e978a07d2e5f';
      }
    });
  } else{
    alert("Заполните все поля!");
  }
});
$(".go-bus-3-1").on("click tap", function(){
  var name = $("#bas-3-1 .name").val().trim();
  var email = $("#bas-3-1 .email").val().trim();
  var phone = $("#bas-3-1 .phone").val().trim();
  var type = "one";
  var price = "14 900";
  if (name && email && phone){
    $.ajax({
      url: "handler.php",
      type: "post",
      data: {type: type, name: name, email: email, phone: phone, price: price},
      success: function(){
        document.location = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=staffpay2&InvId=0&Culture=ru&Encoding=utf-8&Description=Credit&OutSum=14900&SignatureValue=e4be00c4adc6a708ad1ea7a377c27bc5';
      }
    });
  } else{
    alert("Заполните все поля!");
  }
});
$(".go-bus-1-2").on("click tap", function(){
  var name = $("#bas-1-2 .name").val().trim();
  var email = $("#bas-1-2 .email").val().trim();
  var phone = $("#bas-1-2 .phone").val().trim();
  var name_f = $("#bas-1-2 .name-f").val().trim();
  var phone_f = $("#bas-1-2 .phone-f").val().trim();
  var type = "two";
  var price = "4 410";
  if (name && email && phone && name_f && phone_f){
    $.ajax({
      url: "handler.php",
      type: "post",
      data: {type: type, name_f: name_f, phone_f: phone_f, name: name, email: email, phone: phone, price: price},
      success: function(){
        document.location = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=staffpay2&InvId=0&Culture=ru&Encoding=utf-8&Description=MaxPowerTest10&OutSum=4410&SignatureValue=00d50247db888736f00e79dd7ba8fdbd';
      }
    });
  } else{
    alert("Заполните все поля!");
  }
});
$(".go-bus-2-2").on("click tap", function(){
  var name = $("#bas-2-2 .name").val().trim();
  var email = $("#bas-2-2 .email").val().trim();
  var phone = $("#bas-2-2 .phone").val().trim();
  var name_f = $("#bas-2-2 .name-f").val().trim();
  var phone_f = $("#bas-2-2 .phone-f").val().trim();
  var type = "two";
  var price = "35 910";
  if (name && email && phone && name_f && phone_f){
    $.ajax({
      url: "handler.php",
      type: "post",
      data: {type: type, name_f: name_f, phone_f: phone_f, name: name, email: email, phone: phone, price: price},
      success: function(){
        document.location = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=staffpay2&InvId=0&Culture=ru&Encoding=utf-8&Description=FullFriend&OutSum=35910&SignatureValue=992f3a8911999309cc579daf2d242678';
      }
    });
  } else{
    alert("Заполните все поля!");
  }
});
$(".go-bus-3-2").on("click tap", function(){
  var name = $("#bas-3-2 .name").val().trim();
  var email = $("#bas-3-2 .email").val().trim();
  var phone = $("#bas-3-2 .phone").val().trim();
  var name_f = $("#bas-3-2 .name-f").val().trim();
  var phone_f = $("#bas-3-2 .phone-f").val().trim();
  var type = "two";
  var price = "13 410";
  if (name && email && phone && name_f && phone_f){
    $.ajax({
      url: "handler.php",
      type: "post",
      data: {type: type, name_f: name_f, phone_f: phone_f, name: name, email: email, phone: phone, price: price},
      success: function(){
        document.location = 'https://auth.robokassa.ru/Merchant/Index.aspx?MerchantLogin=staffpay2&InvId=0&Culture=ru&Encoding=utf-8&Description=CreditFriend&OutSum=13410&SignatureValue=470ad19dd6da05e86c0895bd1dfd2012';
      }
    });
  } else{
    alert("Заполните все поля!");
  }
});

$("input[type=checkbox]").on("change", function(){ 
  var target = $(this).parent().parent().parent().find(".albut").attr("data-target");
  if (target == "#bas-1-1"){ $(this).parent().parent().parent().find(".albut").attr("data-target", "#bas-1-2"); $(this).parent().parent().parent().find(".bp").text("4 410 р");}
  if (target == "#bas-1-2"){ $(this).parent().parent().parent().find(".albut").attr("data-target", "#bas-1-1"); $(this).parent().parent().parent().find(".bp").text("4 900 р");}
  if (target == "#bas-2-1"){ $(this).parent().parent().parent().find(".albut").attr("data-target", "#bas-2-2"); $(this).parent().parent().parent().find(".bp").text("13 310 р"); }
  if (target == "#bas-2-2"){ $(this).parent().parent().parent().find(".albut").attr("data-target", "#bas-2-1"); $(this).parent().parent().parent().find(".bp").text("15 900 р"); }
  if (target == "#bas-3-1"){ $(this).parent().parent().parent().find(".albut").attr("data-target", "#bas-3-2"); $(this).parent().parent().parent().find(".bp").text("5 310 р");}
  if (target == "#bas-3-2"){ $(this).parent().parent().parent().find(".albut").attr("data-target", "#bas-3-1"); $(this).parent().parent().parent().find(".bp").text("5 900 р");}
});



